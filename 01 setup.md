# Install MySQL Server

To create and manage databases we need a database management system  (DBMS). For this course we will be using MySQL. It is by far the most widely used open source relational DBMS.

We also need a GUI tool to ease our interactions with the server. For this course we will use MySQL Workbench.

Follow the steps bellow to set these up locally (on your laptop/pc)

* Download the MySQL Server MSI from [mysql.com](https://dev.mysql.com/downloads/mysql/). This comes with the MySQL Workbench in the full or developer installation package. You do not need to install Workbench separately if you chose one of those packages. 
* In case you already have a running MySQL Server, you can download the MySQL Workbench MSI separately from [mysql.com](https://dev.mysql.com/downloads/workbench/)
