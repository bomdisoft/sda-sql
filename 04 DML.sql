/* 
	DML stands for Data Manipulation Language. It is used to store and modify (delete, update) data in a database. 

	@author: Adombang Munang Mbomndih
*/


----------------------------------------------------------------------------------------------------------------
-- INSERT – used to add records to a table
----------------------------------------------------------------------------------------------------------------

INSERT INTO user (first_name, last_name, age, active, gender, bio) 
	VALUES('Rick', 'Martins', 16, 1, 'Male', 'I am awesome!');
	
INSERT INTO user VALUES(NULL, 'James', 'King', 25, 1, 'Male', 'bla bla bla', NOW());

INSERT INTO user (first_name, last_name, age, active, gender) VALUES('Laura', 'Coxx', 26, 1, 'Female');
INSERT INTO user (first_name, last_name, age, active, gender) VALUES('Alan', 'Coxx', 26, 1, 'Male');

INSERT INTO user (user_id, first_name, last_name, age, active, gender, bio, started_on) 
	VALUES('Mary', 'Jacobs', 24, 1, 'Female', 'I love dogs and red wine');

----------------------------------------------------------------------------------------------------------------
-- UPDATE – used to update existing data within a table
----------------------------------------------------------------------------------------------------------------
UPDATE user SET active = 0; -- this is not recommended and is disallowed by default on Workbench

UPDATE user SET active = 1 WHERE last_name = 'Coxx';

UPDATE user SET active = 1, age = 41 WHERE user_id = 2;

----------------------------------------------------------------------------------------------------------------
-- DELETE – used to delete records from a table
----------------------------------------------------------------------------------------------------------------
DELETE FROM user WHERE user_id = 2;

----------------------------------------------------------------------------------------------------------------
-- TRUNCATE – used to delete all records from a table, including all allocated spaces
----------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE user;




/* EXCERCISES */

-- 1) How to add multiple records with the same insert statement?
-- 2) How to update users with id of 1 and 3?
-- 3) How to update users with id of 2 or last name of Jacobs?
-- 4) How to update users if their bio contains the letter 'I'?
