/* 
	DML stands for Data Query Language. It is used to retrieve records/information from database. 

	@author: Adombang Munang Mbomndih
*/


USE sda_test_db;

----------------------------------------------------------------------------------------------------------------
-- SELECT – used to retrieve data from a database
----------------------------------------------------------------------------------------------------------------
SELECT * FROM sda_test_db.user;

SELECT * FROM user; -- you don't have to prefix table name if you are selecting from currently active table

SELECT first_name, last_name FROM user;

SELECT first_name AS fName, last_name AS lName FROM user WHERE user_id = 2; -- Alias

SELECT first_name, last_name FROM user WHERE last_name LIKE '_oxx';

SELECT * FROM user WHERE bio LIKE '%dogs%';

SELECT * FROM user WHERE bio LIKE '%dogs%' OR age > 25;

SELECT * FROM user WHERE (bio LIKE '%dogs%' OR age > 25) AND gender = 'Female';

SELECT first_name, last_name,
	CASE
		WHEN age BETWEEN 0 AND 12 THEN 'Kid'
		WHEN age BETWEEN 12 AND 20 THEN 'Teenager'
		ELSE 'Adult'
	END AS age_group
FROM user;

SELECT first_name, last_name, age FROM user ORDER BY last_name ASC;

SELECT first_name, last_name, age FROM user ORDER BY last_name DESC;

SELECT * FROM user LIMIT 2;

SELECT * FROM user LIMIT 2 OFFSET 1; 

SELECT * FROM user LIMIT 1, 2;

----------------------------------------------------------------------------------------------------------------
-- SELECT without table
----------------------------------------------------------------------------------------------------------------
SELECT 5 * 3 FROM DUAL;

SELECT ((8 + (2 * 5)) - 4) / 3;

SELECT 6 % 2 AS six_mod_two;

SELECT MOD(6, 4) AS six_mod_two;

----------------------------------------------------------------------------------------------------------------
-- BUILT-IN FUNCTIONS
----------------------------------------------------------------------------------------------------------------
SELECT COUNT(*) FROM user;

SELECT SUM(age) sum_of_user_ages FROM user;

SELECT last_name, SUM(age) sum_of_user_ages FROM user; -- ??

SELECT last_name, SUM(age) sum_of_user_ages FROM user 
	GROUP BY last_name;

SELECT last_name, SUM(age) sum_of_user_ages FROM user 
	GROUP BY last_name WITH ROLLUP;

SELECT last_name, SUM(age) sum_of_user_ages, COUNT(*) number_of_users FROM user 
	GROUP BY last_name; -- ??

SELECT last_name, SUM(age) sum_of_user_ages, COUNT(*) number_of_users FROM user 
	GROUP BY last_name HAVING number_of_users > 1; -- ??

SELECT CONCAT(first_name, " ", last_name) Employee_Name FROM user;

SELECT CONCAT(first_name, " ", last_name) Name FROM user;

SELECT CONCAT(first_name, " ", last_name) Employee_Name, YEAR(started_on) start_year, 
	MONTH(started_on) start_month, DAY(started_on) start_day FROM user;

SELECT AVG(age) average_age_of_users FROM user;

SELECT ROUND(AVG(age), 2) average_age_of_users FROM user;

SELECT MAX(age) maximum_age_of_users FROM user;




/* EXCERCISES */

-- 1) How to select all users who have a bio?
-- 2) How to select the age of the youngest user?
-- 3) How to select the second oldest user?
