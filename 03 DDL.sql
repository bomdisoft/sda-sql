/* 
	DDL stands for Data Definition Language. It is used to define the structure of data (how the data should reside in the database).

	@author: Adombang Munang Mbomndih
*/


----------------------------------------------------------------------------------------------------------------
-- CREATE – used to create databases and database objects (database, table, index, view, stored procedure, function, trigger)
----------------------------------------------------------------------------------------------------------------

CREATE DATABASE sda_test_db;

CREATE TABLE user(
	user_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	first_name VARCHAR(15) NOT NULL,
	last_name VARCHAR(15) NOT NULL,
	age INT,
	active TINYINT NOT NULL,
	gender enum('Male', 'Female') NOT NULL,
	bio TEXT,
	started_on datetime DEFAULT CURRENT_TIMESTAMP
);

----------------------------------------------------------------------------------------------------------------
-- DROP – used to delete objects from the database or the database itself (views, tables, triggers, procedures, functions etc)
----------------------------------------------------------------------------------------------------------------
DROP TABLE user; -- throws error if table doesn't exist

DROP TABLE IF EXISTS user; -- throws warning if table doesn't exist

DROP DATABASE IF EXISTS sda_test_db;

----------------------------------------------------------------------------------------------------------------
-- ALTER – used to alter/modify the structure of existing databases
----------------------------------------------------------------------------------------------------------------
ALTER TABLE user DROP COLUMN bio;

ALTER TABLE user ADD COLUMN bio TEXT NOT NULL;

ALTER TABLE user CHANGE first_name fName VARCHAR(50) NOT NULL; -- use CHANGE if you wish to update both the column name and definition

ALTER TABLE user RENAME COLUMN bio TO biography; -- use RENAME if you wish to update only the column name (only works on latest version of MySQL)

ALTER TABLE user CHANGE bio biography TEXT; -- use CHANGE if you wish to update both the column name and definition

ALTER TABLE user MODIFY gender enum('Male', 'Female', 'Other') NOT NULL;  -- use MODIFY if you wish to update only the column definition


----------------------------------------------------------------------------------------------------------------
-- RENAME – used to rename objects
----------------------------------------------------------------------------------------------------------------
RENAME TABLE user TO users;



/* EXCERCISES */

-- 1) How to rename multiple columns with 1 query?
-- 2) How to rename a database?
-- 3) Rename the age field on the user table to a field named 'date_of_birth' of type date
