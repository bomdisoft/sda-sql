# Homework

1) Create a school database having tables for users, programs, courses and timetables with the following rules. 
	* A user can either be a lecturer or a student. 
	* A student user can register to study a course. 
	* A lecturer user can register to either study a course or teach a course but not both. 
	* A course can be thought on several days of the week. The days can be decided by an admin.
	* A course can be thought in one or more semesters. The semesters can be decided by an admin.
	* A course can be added to a program. 
	* A student user can register for a program and is automatically registered to all courses in that program.
