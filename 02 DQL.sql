/* 
	DML stands for Data Query Language. It is used to retrieve records/information from database. 

	@author: Adombang Munang Mbomndih
*/


----------------------------------------------------------------------------------------------------------------
-- SELECT – used to retrieve data from a database
----------------------------------------------------------------------------------------------------------------
SELECT DATABASE(); -- shows which database is currently being used. Returns null if no database has been selected

----------------------------------------------------------------------------------------------------------------
-- SHOW – provides information about databases, tables, columns, or status information about the server
----------------------------------------------------------------------------------------------------------------
SHOW DATABASES; -- returns a list of all databases

SHOW TABLES; -- returns a list of all tables in the current database

SHOW COLUMNS FROM city;

SHOW INDEX FROM country;

SHOW CREATE TABLE countrylanguage; -- shows the query that was used to create the table

----------------------------------------------------------------------------------------------------------------
-- DESCRIBE – used to retrieve information about the structure of a table
----------------------------------------------------------------------------------------------------------------
DESCRIBE city; -- same as SHOW COLUMNS FROM <table_name>

DESC city; -- same as DESCRIBE city

----------------------------------------------------------------------------------------------------------------
-- EXPLAIN – provides information about how MySQL executes statements
----------------------------------------------------------------------------------------------------------------
EXPLAIN SELECT DATABASE();



/* EXCERCISES */

-- 1) How many indexes are on the city table?
-- 2) How many non-unique indexes are on the city table?
-- 3) What's the default value of the continent field on the country table?
-- 4) What's the engine of the country table?
